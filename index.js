const express = require("express");
const port = 4000;
const app = express();

app.use(express.json());

//mock data
let users = [
	{
		username: "TStark3000",
		email: "starksindustries@mail.com",
		password: "notPeterPark",
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker",
	}
];

let items = [
	{
		name: "Mjolnir",
		price: 50000,
		isActive: true
	},
	{
		name: "Vibranium Shield",
		price: 70000,
		isActive: true
	}
];

app.get("/", (request, response) => {
	response.send(`Hello from my first expressJS API`);
});

//Activity
app.get("/greeting", (request, response) => {
	response.send(`Hello from Batch203-Ang`);
});

app.get("/users", (request, response) => {
	response.send(users);
});

app.post("/users", (request, response) => {
	console.log(request.body);
	let newUser = {
		username: request.body.username,
		email: request.body.email,
		password: request.body.password
	}
	users.push(newUser);
	console.log(users);

	response.send(users);
});

app.delete("/users", (request, response) => {
	users.pop();
	response.send(users);
});

app.put("/users/:index", (request, response) => {
	console.log(request.body);
	console.log(request.params);

	let index = parseInt(request.params.index);
	users[index].password = request.body.password;

	response.send(users[index]);
});


/*

	>> Create a new route to get and send items array in the client (GET ALL ITEMS)

	>> Create a new route to create and add a new item object in the items array (CREATE ITEM)
	        >> send the updated items array in the client
	        >> check the post method route for our users for reference

	>> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
	        >> pass the index number of the item that you want to update in the request params
	        >> add the price update in the request body
	        >> reassign the new price from our request body
	        >> send the updated item in the client

	>> Create a new collection in Postman called s34-activity
	>> Save the Postman collection in your s34 folder

*/

app.get("/items", (request, response) => {
	response.send(items);
});

app.post("/items", (request, response) => {
	let newItem = {
		name: request.body.name,
		price: request.body.price,
		isActive: request.body.isActive
	};
	items.push(newItem);
	response.send(items);
});

app.put("/items/:index", (request, response) => {
	let index = parseInt(request.params.index);
	items[index].price = request.body.price;
	response.send(items[index]);
});


//port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));

